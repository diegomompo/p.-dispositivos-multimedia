﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02__WindowsFormIterativas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int numero;

            labelResultado.Text = "";
            numero = Int32.Parse(textBoxNumero.Text);

            for(int i = 1; i<=numero; i++)
            {
                labelResultado.Text += i;

                if (i != numero)
                {
                    labelResultado.Text +=  " - ";
                }
            }
        }

        private void buttonImprimirTabla_Click(object sender, EventArgs e)
        {
            int[] tabla = { 11, 22, 33, 44, 55 };

            //for tradicional
            /*for (int i = 0; i < 5; i++)
            {
                labelResultado.Text += tabla[i] + " ";
            }*/

            //foreach para colecciones

            foreach(int numero in tabla)
            {
                labelResultado.Text += numero + " ";
            }
        }

        private void buttonImprimir_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
