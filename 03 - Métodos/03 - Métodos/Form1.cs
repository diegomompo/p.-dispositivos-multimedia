﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03___Métodos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ImprimirNumeros(int n1, int n2)
        {
            labelResultado.Text += " N1 = " + n1 + " N2 = " + n2 + "\r\n";
        }

        private int sumarV1(int n1, int n2) {

            int res;
            res = n1 + n2;
            n1 = 0;
            n2 = 0;
            return res;
        }
        private int sumarV2(int n1, ref int n2)
        {

            int res;
            res = n1 + n2;
            n1 = 0;
            n2 = 0;
            return res;
        }
        private void sumarV3(int n1, int n2, out int res)
        {
            res = n1 + n2;
            n1 = 0;
            n2 = 0;

        }
        private void Sumar1_Click(object sender, EventArgs e)
        {
            int numero1, numero2;

            numero1 = Int32.Parse(Numero1.Text);
            numero2 = Int32.Parse(Numero2.Text);

            ImprimirNumeros(numero1, numero2);
            int resultado = sumarV1(numero1, numero2);
            labelResultado.Text += " " + resultado + "\r\n";

        }

        private void sumar2_Click(object sender, EventArgs e)
        {
            int numero1, numero2;

            numero1 = Int32.Parse(Numero1.Text);
            numero2 = Int32.Parse(Numero2.Text);

            int resultado = sumarV2(numero1, ref numero2);
            labelResultado.Text += " " + resultado;
        }

        private void sumar3_Click(object sender, EventArgs e)
        {
            int numero1, numero2, resultado;

            numero1 = Int32.Parse(Numero1.Text);
            numero2 = Int32.Parse(Numero2.Text);

            sumarV3(numero1, numero2, out resultado);
            labelResultado.Text += " " + resultado;
        }
    }
    }

