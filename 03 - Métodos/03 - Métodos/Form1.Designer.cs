﻿namespace _03___Métodos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Sumar1 = new System.Windows.Forms.Button();
            this.Numero1 = new System.Windows.Forms.TextBox();
            this.Numero2 = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.sumar2 = new System.Windows.Forms.Button();
            this.sumar3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Sumar1
            // 
            this.Sumar1.Location = new System.Drawing.Point(318, 44);
            this.Sumar1.Name = "Sumar1";
            this.Sumar1.Size = new System.Drawing.Size(174, 58);
            this.Sumar1.TabIndex = 0;
            this.Sumar1.Text = "Sumar V1";
            this.Sumar1.UseVisualStyleBackColor = true;
            this.Sumar1.Click += new System.EventHandler(this.Sumar1_Click);
            // 
            // Numero1
            // 
            this.Numero1.Location = new System.Drawing.Point(76, 130);
            this.Numero1.Name = "Numero1";
            this.Numero1.Size = new System.Drawing.Size(100, 40);
            this.Numero1.TabIndex = 1;
            // 
            // Numero2
            // 
            this.Numero2.Location = new System.Drawing.Point(197, 130);
            this.Numero2.Name = "Numero2";
            this.Numero2.Size = new System.Drawing.Size(100, 40);
            this.Numero2.TabIndex = 2;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(571, 95);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(208, 33);
            this.labelResultado.TabIndex = 3;
            this.labelResultado.Text = "El resultado es";
            // 
            // sumar2
            // 
            this.sumar2.Location = new System.Drawing.Point(318, 112);
            this.sumar2.Name = "sumar2";
            this.sumar2.Size = new System.Drawing.Size(174, 58);
            this.sumar2.TabIndex = 0;
            this.sumar2.Text = "Sumar V2";
            this.sumar2.UseVisualStyleBackColor = true;
            this.sumar2.Click += new System.EventHandler(this.sumar2_Click);
            // 
            // sumar3
            // 
            this.sumar3.Location = new System.Drawing.Point(318, 176);
            this.sumar3.Name = "sumar3";
            this.sumar3.Size = new System.Drawing.Size(174, 58);
            this.sumar3.TabIndex = 0;
            this.sumar3.Text = "Sumar V3";
            this.sumar3.UseVisualStyleBackColor = true;
            this.sumar3.Click += new System.EventHandler(this.sumar3_Click);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1455, 619);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.Numero2);
            this.Controls.Add(this.sumar3);
            this.Controls.Add(this.sumar2);
            this.Controls.Add(this.Numero1);
            this.Controls.Add(this.Sumar1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Form1";
            this.Text = "ñ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Sumar1;
        private System.Windows.Forms.TextBox Numero1;
        private System.Windows.Forms.TextBox Numero2;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button sumar2;
        private System.Windows.Forms.Button sumar3;
    }
}

