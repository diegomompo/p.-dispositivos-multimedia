﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01__ConsoleApplicationTipos
{
    class Program
    {
        static void Main(string[] args)
        {
            Type tipo;
            char letra = 'm';

            tipo = typeof(string);

            Console.WriteLine("El nombre es " + tipo.Name);
            Console.WriteLine("El nombre es " + tipo.FullName);
            Console.WriteLine("Caminante, Argentino, ED, DI MARÍA");

            if ((3 + 4) is Int32)
            {
                Console.WriteLine("es entero");
            }
            else
            {
                Console.WriteLine("no es entero");
            }

            double numero = 32.5;

            Console.WriteLine("El número es " + numero);
            Console.WriteLine("El número convertido es " + (int) numero);

            switch (letra)
            {
                case 'a': Console.WriteLine("viva aitana");
                    break;
            }
            

            Console.ReadKey();
        }
    }
}
